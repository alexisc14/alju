<?php

namespace App\Http\Controllers;

use App\Models\Citation;
use App\Models\Event;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $events = Event::all();
        $citation = Citation::all()->random();
        return view('index', compact('events', 'citation'));
    }

    public function dashboard()
    {
        return view('layouts.dashboard');
    }
}
