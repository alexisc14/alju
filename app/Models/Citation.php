<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Citation extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'citation';
    protected $primaryKey = 'id';
}
