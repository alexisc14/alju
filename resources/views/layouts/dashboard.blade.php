@extends('layouts.home')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" id="card-header-name">Bienvenue {{ Auth::user()->name }} ! </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <select id="selectAction" name="selectAction">
                                        <option value="" selected disabled>Sélectionner l'action</option>
                                        <option value="citation">Citation</option>
                                        <option value="event">Événement</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div id="div-append"></div>
            </div>
        </div>

    </div>
@endsection
@section('scripts')
    <script>
        let selectAction = $("#selectAction")
        selectAction.on('change', function(e) {
            if (selectAction[0].value === "citation") {
                var url = '{{ route('citation.store') }}'
                $('#div-append').html(
                    '<br>'+
                    '<div class="card">' +
                    '<div class="card-header">Créer un mot de l\'amour</div>' +
                    '<div class="card-body">' +
                    '<form action="' + url + '" method="POST">' +
                    '@csrf' +
                    '<div class="form-group row">' +
                    '<div class="col-md-12">' +
                    '<textarea id="citation" class="form-control" name="citation" required placeholder="Ecrire le mot"></textarea>' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group row">' +
                    '<div class="col-md-12">' +
                    '<button type="submit" class="btn btn-primary">' +
                    'Créer' +
                    '</button>' +
                    '</div>' +
                    '</div>' +
                    '</form>' +
                    '</div>' +
                    '</div>'
                )
            } else {
                var url = '{{ route('event.store') }}'
                $('#div-append').html(
                    '<br>'+
                    '<div class="card">' +
                    '<div class="card-header">Créer un événement</div>' +
                    '<div class="card-body">' +
                    '<form action="' + url + '" method="POST">' +
                    '@csrf' +
                    '<div class="form-group row">' +
                    '<div class="col-md-12">' +
                    '<label class="col-form-label">Nom</label>'+
                    '<input type="text" id="nameEvent" class="form-control" name="nameEvent" placeholder="Entrer le nom de l\'événement" required>' +
                    '</div>' +
                    '</div>'+
                    '<div class="form-group row">' +
                    '<div class="col-md-12">' +
                    '<label class="col-form-label">Date prévue</label>'+
                    '<input type="date" id="dateEvent" class="form-control" name="dateEvent" required>' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group row">' +
                    '<div class="col-md-12">' +
                    '<label class="col-form-label">Description</label>'+
                    '<textarea id="descEvent" class="form-control" name="descEvent" required placeholder="Entrer une description de l\'événement"></textarea>' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group row">' +
                    '<div class="col-md-12">' +
                    '<button type="submit" class="btn btn-primary">' +
                    'Créer' +
                    '</button>' +
                    '</div>' +
                    '</div>' +
                    '</form>' +
                    '</div>' +
                    '</div>'
                )
            }

        })
    </script>
@endsection
