<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.header.header')
<body>
    <!-- Preloader Start -->
    @include('layouts.navbar.navbar')
    <div class="content">
        @yield('content')
    </div>
    @include('layouts.footer.footer-scripts')
</body>
</html>
