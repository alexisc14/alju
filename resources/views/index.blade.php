@extends('layouts.home')

@section('content')
    <main>
        <!--? Our Story Start -->
        <div class="Our-story-area story-padding">
            <div class="container">
                <!-- Section Tittle -->
                <div class="row d-flex justify-content-center">
                    <div class="col-xl-7 col-lg-8">
                        <div class="section-tittle text-center mb-70">
                            <h2>Petit mot de l'amour ❤️</h2>
                            <img src="{{ asset('assets/img/gallery/tittle_img.png') }}" alt="">
                            <p>{{ $citation->mot }}</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Our Story End -->
        @foreach ($events as $event)
            @php
                $datetime1 = date_create(date('Y-m-d h:i:s'));
                $datetime2 = date_create($event->date_execution);
                $interval = date_diff($datetime1, $datetime2);
            @endphp
            <!--? Services Start -->
            <section class="pricing-card-area section-padding30 section-bg"
                data-background="assets/img/gallery/section_bg1.png">
                <div class="container">
                    <!-- Section Tittle -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-8">
                            <div class="section-tittle text-center mb-70">
                                <h2>{{ $event->name }}</h2>
                                <img src="{{ asset('assets/img/gallery/tittle_img.png') }}" alt="">
                                <p>{{ $event->description }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Services Card End -->
            <!--? Count Down Start -->
            <div class="count-down-area">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-12 col-md-12">
                            <div class="count-down-wrapper"
                                data-background="{{ asset('assets/img/gallery/section_bg2.png') }}">
                                <div class="row justify-content-between">
                                    <div class="col-lg-2 col-md-6 col-sm-6">
                                        <!-- Counter Up -->
                                        <div class="single-counter text-center">
                                            <span class="counter">{{ $interval->m }}</span>
                                            <p>Mois</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-6 col-sm-6">
                                        <!-- Counter Up -->
                                        <div class="single-counter text-center">
                                            <span class="counter">{{ $interval->d }}</span>
                                            <p>@if ($interval->d > 0)Jours @else Jour @endif</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-6 col-sm-6">
                                        <!-- Counter Up -->
                                        <div class="single-counter active text-center">
                                            <span class="counter">{{ $interval->h }}</span>
                                            <p>@if ($interval->h > 0) Heures @else Heure @endif</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-6 col-sm-6">
                                        <!-- Counter Up -->
                                        <div class="single-counter text-center">
                                            <span class="counter">{{ $interval->i }}</span>
                                            <p>@if ($interval->i > 0) Minutes @else Minute @endif</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </main>
@endsection
